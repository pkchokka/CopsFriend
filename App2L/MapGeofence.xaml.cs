﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Devices.Geolocation;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage.Streams;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Maps;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace CopsCompanion
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MapGeofence : Page
    {
        List<BasicGeoposition> geoTappedPositions;
        MapPolygon mapPolygon = null;
        int tapCount = 0;
        public MapGeofence()
        {
            this.InitializeComponent();
            btnCapturePoints.Content = "Select Points";
        }

        private bool isSelectedSelected = false;
        private void mapFence_MapTapped(MapControl sender, MapInputEventArgs args)
        {
            if (isSelectedSelected)
            {
                tapCount++;
                // Geopoint p =  MapControl.GetLocation(args);
                BasicGeoposition p = args.Location.Position;
                geoTappedPositions.Add(p);
                mapFence.MapElements.Add(new MapIcon()
                {
                    Title = "Selected Point " + tapCount,
                    NormalizedAnchorPoint = new Point(0.5, 1.0),
                    Image = RandomAccessStreamReference.CreateFromUri(new Uri("ms-appx:///Assets/mappin.png")),
                    //Image = new MapIconImage new Uri("ms-appx:///Assets/MapPin.png", UriKind.RelativeOrAbsolute),
                    Location = new Geopoint(new BasicGeoposition()
                    {
                        Latitude = p.Latitude,
                        Longitude = p.Longitude
                    })
                });
            }
        }

        private void btnDrawPolygon_Click(object sender, RoutedEventArgs e)
        {
            mapPolygon = new MapPolygon();
            mapPolygon.Path = new Geopath(geoTappedPositions);
            mapPolygon.ZIndex = 1;
            mapPolygon.FillColor = Colors.Transparent;
            mapPolygon.StrokeColor = Colors.Blue;
            mapPolygon.StrokeThickness = 3;
            mapPolygon.StrokeDashed = true;
            mapFence.MapElements.Add(mapPolygon);
            btnCapturePoints.Content = "Clear Points";
        }

        private void btnCapturePoints_Click(object sender, RoutedEventArgs e)
        {
            isSelectedSelected = true;
            mapFence.MapElements.Clear();
            geoTappedPositions = new List<BasicGeoposition>();
            tapCount = 0;
            btnCapturePoints.Content = "Select Points";

        }

        private void mapFence_Loaded(object sender, RoutedEventArgs e)
        {

        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            BasicGeoposition cityPosition = new BasicGeoposition() { Latitude = 17.4354874, Longitude = 78.4438218 };
            Geopoint cityCenter = new Geopoint(cityPosition);

            mapFence.Center = cityCenter;
            mapFence.ZoomLevel = 12;
            mapFence.LandmarksVisible = true;          
        }

        private void mapFence_Holding(object sender, HoldingRoutedEventArgs e)
        {
            // send the incident record numbers and display therecords in Incident.xaml
            this.Frame.Navigate(typeof(Incident), 1);
        }

        private void mapFence_DoubleTapped(object sender, DoubleTappedRoutedEventArgs e)
        {
            // send the incident record numbers and display therecords in Incident.xaml

            this.Frame.Navigate(typeof(Incident), 1);

        }

        private void btnShowIncidents_Click(object sender, RoutedEventArgs e)
        {
            Locations s = new Locations();
            List<MapIcon> locList = s.GetLocations(true,true);
            BasicGeoposition inciPoint;
            bool isPresent = false;

            foreach (MapIcon loc in locList)
            {
                inciPoint = new BasicGeoposition()
                {
                    Latitude = loc.Location.Position.Latitude,
                    Longitude = loc.Location.Position.Longitude
                };

                isPresent = false;
                isPresent = IsIncidePolygon(inciPoint.Latitude, inciPoint.Longitude);

                if (isPresent)
                {
                    mapFence.MapElements.Add(loc);
                }
            }
        }

        private bool IsIncidePolygon(double lat, double lon)
        {
            // List<BasicGeoposition> polyPoints = (List<BasicGeoposition>)mapPolygon.Path.Positions;
            List<BasicGeoposition> polyPoints = geoTappedPositions;
            bool isPre = false;
            int i, j;
            int count = polyPoints.Count;


            //for (i = 0, j = geoTappedPositions.Count; i < geoTappedPositions.Count; j = i++)
            //{
            //    if ((((geoTappedPositions[i].Latitude <= lat) && (lat < geoTappedPositions[j].Latitude))
            //            || ((geoTappedPositions[j].Latitude <= lat) && (lat < geoTappedPositions[i].Latitude)))
            //                && (lon < (geoTappedPositions[j].Longitude - geoTappedPositions[i].Longitude) * (lat - geoTappedPositions[i].Latitude)
            //                    / (geoTappedPositions[j].Latitude - geoTappedPositions[i].Latitude) + geoTappedPositions[i].Latitude))

            //        isPre = !isPre;
            //}

            i = -1;
            j = count - 1;
            while ((i += 1) < count)
            {
                if ((polyPoints[i].Longitude <= lon && lon < polyPoints[j].Longitude) ||
                   (polyPoints[j].Longitude <= lon && lon < polyPoints[i].Longitude))
                {
                    if (lat < (polyPoints[j].Latitude - polyPoints[i].Latitude) * (lon - polyPoints[i].Longitude)
                        / (polyPoints[j].Longitude - polyPoints[i].Longitude) + polyPoints[i].Latitude)
                    {
                        isPre = !isPre;
                    }

                }
                j = i;
            }

            return isPre;
        }

        private void btnViewIncidents_Click(object sender, RoutedEventArgs e)
        {
            // send the incident record numbers and display therecords in Incident.xaml

            this.Frame.Navigate(typeof(Incident), 1);
        }
    }
}
