﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Devices.Geolocation;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Services.Maps;
using Windows.Storage.Streams;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Maps;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace CopsCompanion
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MapViewer : Page
    {
        private string typeOfLocation;
        private IncidentName selectedIncidentName;
        private IncidentOffense selectedIncidentOffense;
        BasicGeoposition currentPosition;
        bool showRouteOptionEnabled = false;
        bool isIncidentDetailsSearch = false;
        bool isSuspect = false;
        Geoposition startRoutePoint;
        BasicGeoposition bgpStartRoutePosition;

        BasicGeoposition destinationPoint = new BasicGeoposition();

        public MapViewer()
        {
            this.InitializeComponent();
        }

        private List<MapIcon> GetMapIcons()
        {
            List<MapIcon> mapItems = new List<MapIcon>();
            BasicGeoposition snPosition = new BasicGeoposition() { Latitude = 17.3616, Longitude = 78.4747 };
            Geopoint snPoint = new Geopoint(snPosition);

            // Create a MapIcon.
            MapIcon mapIcon1 = new MapIcon();
            mapIcon1.Location = snPoint;
            mapIcon1.NormalizedAnchorPoint = new Point(0.5, 1.0);
            mapIcon1.Title = "Charminar";
            mapIcon1.ZIndex = 0;

            // Add the MapIcon to the map.
           mapItems.Add(mapIcon1);
            BasicGeoposition snPosition1 = new BasicGeoposition() { Latitude = 17.371265, Longitude = 78.478109 };
            Geopoint snPoint1 = new Geopoint(snPosition1);

            // Create a MapIcon.
            MapIcon mapIcon2 = new MapIcon();
            mapIcon2.Location = snPoint1;
            mapIcon2.NormalizedAnchorPoint = new Point(0.5, 1.0);
            mapIcon2.Title = "Salarjung Museum";
            mapIcon2.ZIndex = 0;

            // Add the MapIcon to the map.
            mapItems.Add(mapIcon2);
            return mapItems;

        }


        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            BasicGeoposition cityPosition = new BasicGeoposition() { Latitude = 17.4354874, Longitude = 78.4438218 };
            Geopoint cityCenter = new Geopoint(cityPosition);

            mapIncident.Center = cityCenter;
            mapIncident.ZoomLevel = 12;
            mapIncident.LandmarksVisible = true;

            object[] parameters = (object[])e.Parameter;
            switch (parameters[0].ToString())
            {
                case "SUSPECT":
                    selectedIncidentName = (IncidentName)parameters[1];
                    isIncidentDetailsSearch = false;
                    isSuspect = true;
                    break;
                case "OFFENSE":
                    selectedIncidentOffense = (IncidentOffense)parameters[1];
                    isIncidentDetailsSearch = true;
                    isSuspect = false;
                    break;
                default:
                    break;
            }


        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            Locations s = new Locations();           

            foreach(MapIcon m in s.GetLocations(isIncidentDetailsSearch, isSuspect))
            {
                mapIncident.MapElements.Add(m);
            }
            
        }

        private async void ShowMyLocationOnTheMap()
        {
            Geolocator myGeolocator = new Geolocator();
            Geoposition myGeoposition = await myGeolocator.GetGeopositionAsync();
            currentPosition = new BasicGeoposition();
            currentPosition.Latitude = myGeoposition.Coordinate.Latitude;
            currentPosition.Longitude = myGeoposition.Coordinate.Longitude;
            currentPosition.Altitude = myGeoposition.Coordinate.Altitude.Value;
            
        }

        private void btnDrawPolygon_Click(object sender, RoutedEventArgs e)
        {
            double centerLatitude = mapIncident.Center.Position.Latitude;
            double centerLongitude = mapIncident.Center.Position.Longitude;
            MapPolygon mapPolygon = new MapPolygon();
            mapPolygon.Path = new Geopath(new List<BasicGeoposition>() {
                new BasicGeoposition() {Latitude=centerLatitude+1, Longitude=centerLongitude-0.001 },
                new BasicGeoposition() {Latitude=centerLatitude-0.0005, Longitude=centerLongitude-0.001 },
                new BasicGeoposition() {Latitude=centerLatitude-0.0005, Longitude=centerLongitude+0.001 },
                new BasicGeoposition() {Latitude=centerLatitude+0.0005, Longitude=centerLongitude+0.001 },

            });

            mapPolygon.ZIndex = 1;
            mapPolygon.FillColor = Colors.Transparent;
            mapPolygon.StrokeColor = Colors.Blue;
            mapPolygon.StrokeThickness = 3;
            mapPolygon.StrokeDashed = false;
            mapIncident.MapElements.Add(mapPolygon);

        }

        private void btnCapturePoints_Click(object sender, RoutedEventArgs e)
        {
           
        }

        List<BasicGeoposition> position = new List<BasicGeoposition>();
        
        private async void mapIncident_MapTapped(MapControl sender, MapInputEventArgs args)
        {
            if (!showRouteOptionEnabled)
            {
                // Geopoint p =  MapControl.GetLocation(args);
                //BasicGeoposition p = args.Location.Position;
                //position.Add(p);
                //mapIncident.MapElements.Add(new MapIcon()
                //{
                //    Title = "Selected Point",
                //    NormalizedAnchorPoint = new Point(0.5, 1.0),
                //    Image = RandomAccessStreamReference.CreateFromUri(new Uri("ms-appx:///Assets/mappin.png")),
                //    //Image = new MapIconImage new Uri("ms-appx:///Assets/MapPin.png", UriKind.RelativeOrAbsolute),
                //    Location = new Geopoint(new BasicGeoposition()
                //    {
                //        Latitude = p.Latitude,
                //        Longitude = p.Longitude
                //    })
                //});
            }
            else
            {
                mapIncident.MapElements.Clear();
                position = new List<BasicGeoposition>();
                destinationPoint = new BasicGeoposition();
                destinationPoint = args.Location.Position;
                position.Add(destinationPoint);
                mapIncident.MapElements.Add(new MapIcon()
                {
                    Title = "CurrentPostion",
                    NormalizedAnchorPoint = new Point(0.5, 1.0),
                    Image = RandomAccessStreamReference.CreateFromUri(new Uri("ms-appx:///Assets/StolenAutoPin.png")),
                    //Image = new MapIconImage new Uri("ms-appx:///Assets/MapPin.png", UriKind.RelativeOrAbsolute),
                    Location = new Geopoint(bgpStartRoutePosition)
                });
                mapIncident.MapElements.Add(new MapIcon()
                {
                    Title = "Selected Incident",
                    NormalizedAnchorPoint = new Point(0.5, 1.0),
                    Image = RandomAccessStreamReference.CreateFromUri(new Uri("ms-appx:///Assets/OtherPin.png")),

                    //Image = new MapIconImage new Uri("ms-appx:///Assets/MapPin.png", UriKind.RelativeOrAbsolute),
                    Location = new Geopoint(new BasicGeoposition()
                    {
                        Latitude = destinationPoint.Latitude,
                        Longitude = destinationPoint.Longitude
                    })
                });
                await GetTheRouteDisplayedOnMap(destinationPoint);

            }
        }

        
        
        private async void btnShowRoute_Click(object sender, RoutedEventArgs e)
        {
            Geolocator geolocator = new Geolocator { DesiredAccuracyInMeters = 0 };
            Geoposition startRoutePoint = await geolocator.GetGeopositionAsync();
            bgpStartRoutePosition = new BasicGeoposition();

            bgpStartRoutePosition.Latitude = startRoutePoint.Coordinate.Latitude;
            bgpStartRoutePosition.Longitude = startRoutePoint.Coordinate.Longitude;

            //mapIncident.MapElements.Add(new MapIcon()
            //{
            //    Title = "Current Postion",
            //    NormalizedAnchorPoint = new Point(0.5, 1.0),
            //    Image = RandomAccessStreamReference.CreateFromUri(new Uri("ms-appx:///Assets/currentLocation.png")),

            //    //Image = new MapIconImage new Uri("ms-appx:///Assets/MapPin.png", UriKind.RelativeOrAbsolute),
            //    Location = new Geopoint(bgpStartRoutePosition)
            //});
            // open the Popup if it isn't open already 
            if (!StandardPopup.IsOpen) { StandardPopup.IsOpen = true; }
            if (StandardPopup.IsOpen)
            {
                showRouteOptionEnabled = true;
             //   mapIncident.MapElements.Clear();

            }
        }

        private async System.Threading.Tasks.Task GetTheRouteDisplayedOnMap( BasicGeoposition destination)
        {
            // Start at Microsoft in Redmond, Washington.

            // End at the city of Seattle, Washington.
            BasicGeoposition endLocation = destination;


            // Get the route between the points.
            MapRouteFinderResult routeResult =
                  await MapRouteFinder.GetDrivingRouteAsync(
                  new Geopoint(bgpStartRoutePosition),
                  new Geopoint(endLocation),
                  MapRouteOptimization.Time,
                  MapRouteRestrictions.None);

            if (routeResult.Status == MapRouteFinderStatus.Success)
            {
                // Use the route to initialize a MapRouteView.
                MapRouteView viewOfRoute = new MapRouteView(routeResult.Route);
                viewOfRoute.RouteColor = Colors.Yellow;
                viewOfRoute.OutlineColor = Colors.Black;

                // Add the new MapRouteView to the Routes collection
                // of the MapControl.
                mapIncident.Routes.Add(viewOfRoute);


                // Fit the MapControl to the route.
                await mapIncident.TrySetViewBoundsAsync(
                      routeResult.Route.BoundingBox,
                      null,
                      Windows.UI.Xaml.Controls.Maps.MapAnimationKind.None);
            }
        }

        private void ClosePopupClicked(object sender, RoutedEventArgs e)
        {
            // if the Popup is open, then close it 
            if (StandardPopup.IsOpen) { StandardPopup.IsOpen = false; }
        }
    }
}
