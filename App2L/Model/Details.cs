﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CopsCompanion
{
    public class Details
    {
        public ObservableCollection<IncidentName> GetIncidentNames(int inciRecnum)
        {
            // returns Incidents Objects

            // We have to get the Incidents based on Investigating Officer Recnum from INCIDENT table.
            ObservableCollection<IncidentName> inciNamesList = new ObservableCollection<IncidentName>();

            if (true)
            {
                inciNamesList.Add(new IncidentName()
                {
                    Age = (23),
                    FirstName = "Jeffery ",
                    LastName = "Jackson",
                    NameRecnum = 1589,
                    NameType = "SUSPECT",
                    Sex = "M",
                    ImagePath = "Assets/off1.jpg"
                });
                inciNamesList.Add(new IncidentName()
                {
                    Age = (23),
                    FirstName = "Gundapa ",
                    LastName = "Reddy",
                    NameRecnum = 1590,
                    NameType = "SUSPECT",
                    Sex = "M",
                    ImagePath = "Assets/off3.jpg"
                });
                inciNamesList.Add(new IncidentName()
                {
                    Age = (23),
                    FirstName = "Tiger ",
                    LastName = "Woods",
                    NameRecnum = 1591,
                    NameType = "SUSPECT",
                    Sex = "M",
                    ImagePath = "Assets/off5.jpg"
                });
                inciNamesList.Add(new IncidentName()
                {
                    Age = (23),
                    FirstName = "David ",
                    LastName = "Beckham",
                    NameRecnum = 1592,
                    NameType = "VICTIM",
                    Sex = "M",
                    ImagePath = "Assets/off2.jpg"
                });
                inciNamesList.Add(new IncidentName()
                {
                    Age = (23),
                    FirstName = "Sean ",
                    LastName = "Holt",
                    NameRecnum = 1593,
                    NameType = "VICTIM",
                    Sex = "M",
                    ImagePath = "Assets/off4.jpg"
                });
                inciNamesList.Add(new IncidentName()
                {
                    Age = (23),
                    FirstName = "Tony ",
                    LastName = "Parker ",
                    NameRecnum = 1594,
                    NameType = "VICTIM",
                    Sex = "M",
                    ImagePath = "Assets/off6.jpg"
                });
                // Assuming this Incident has 3 names
                //for (int i = 0; i < 6; i++)
                //{
                //    if (i % 2 == 0)
                //    {
                //        inciNamesList.Add(new IncidentName()
                //        {
                //            Age = (i + 10),
                //            FirstName = "Oliver ",
                //            LastName = "O' Stone",
                //            NameRecnum = i * 1000,
                //            NameType = "SUSPECT",
                //            Sex = "M",
                //            ImagePath = "Assets/off" + (i + 1) + ".jpg"
                //        });
                //    }
                //    else
                //    {
                //        inciNamesList.Add(new IncidentName()
                //        {
                //            Age = i + 50,
                //            FirstName = "Kobe ",
                //            LastName = "Bryant",
                //            NameRecnum = i * 1000,
                //            NameType = "VICTIM",
                //            Sex = "M",
                //            ImagePath = "Assets/off" + (i + 1) + ".jpg"
                //        });
                //    }
                //}
            }
            else
            {
                // Write code to get Incidents list from RMS DB
            }

            return inciNamesList;
        }
      
        public ObservableCollection<IncidentOffense> GetIncidentOffenses(int inciRecnum)
        {
            // returns Incidents Objects

            // We have to get the Incidents based on Investigating Officer Recnum from INCIDENT table.
            ObservableCollection<IncidentOffense> inciOffenseList = new ObservableCollection<IncidentOffense>();

            if (true)
            {
                // Assuming this Icident has 2 Offenses data
               
                    inciOffenseList.Add(new IncidentOffense()
                    {
                        InciOffenseRecnum =1456200,
                        OffenseCode = "13A",
                        OffenseDescription = "Aggravated Assault",
                        OffenseRecnum = 1560,
                        OffenseSeverity = "HIGH"
                    });
                   inciOffenseList.Add(new IncidentOffense()
                   {
                       InciOffenseRecnum = 1456200,
                       OffenseCode = "09A",
                       OffenseDescription = "Homicide",
                       OffenseRecnum = 1561,
                       OffenseSeverity = "HIGH"
                   });


            }
            else
            {
                // Write code to get Incidents list from RMS DB
            }

            return inciOffenseList;
        }
       
        public ObservableCollection<IncidentLocation> GetIncidentLocation(int inciRecnum)
        {
            // returns Incidents Objects

            // We have to get the Locaton info from Incident based on MASTLOCATION Recnum from INCIDENT table.
            ObservableCollection<IncidentLocation> inciLocList = new ObservableCollection<IncidentLocation>();

            if (true)
            {
                // Primary location woul dbe only one, hence send a single location
                for (int i = 0; i < 1; i++)
                {
                    inciLocList.Add(new IncidentLocation()
                    {
                        Address =  " Kalasi Guda-Parklane",
                        City = "Secunderabad",
                        Latitude = 17.44327,
                        Longitude = 78.4887,
                        State = "TG",
                        Zip = 500040,
                    });
                }
            }
            else
            {
                // Write code to get Incidents list from RMS DB
            }

            return inciLocList;
        }
    }

}
