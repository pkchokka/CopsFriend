﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CopsCompanion
{
    public class IncidentName
    {
        public int Age { get; set; }       
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string NameType { get; set; }
        public int NameRecnum { get; set; }
        public string Sex { get; set; }
        public string ImagePath { get; set; }
    }


    public class IncidentLocation
    {
        public string Address { get; set; }
        public string City { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string State { get; set; }
        public int Zip { get; set; }        
    }


    public class IncidentOffense
    {
        public int InciOffenseRecnum { get; set; }
        public string OffenseCode { get; set; }
        public string OffenseDescription { get; set; }
        public int OffenseRecnum { get; set; }
        public string OffenseSeverity { get; set; }

    }
}
