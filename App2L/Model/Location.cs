﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Geolocation;
using Windows.Foundation;

namespace CopsCompanion
{
    public class Location
    {
        public Location()
        {
            this.MoreInfo = "Incident Location";
            this.NormalizedAnchorPoint = new Point(0.5, 1);
        }
        public string DisplayName { get; set; }        
        public Uri ImageSourceUri { get; set; }
        public Geopoint LocationPoint { get; set; }
        public int LocationRecnum { get; set; }
        public string MoreInfo { get; set; }
        public Point NormalizedAnchorPoint { get; set; }
        
    }
}
