﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CopsCompanion
{
    public class Incidents
    {
        public ObservableCollection<IncidentDetail> GetIncidents(int officerRecnum)
        {
            // returns Incidents Objects

            // We have to get the Incidents based on Investigating Officer Recnum from INCIDENT table.
            ObservableCollection<IncidentDetail> inciList = new ObservableCollection<IncidentDetail>();

            if (true)
            {
                inciList.Add(new IncidentDetail()
                {
                    IncidentDate = DateTime.Today.AddDays(-30),
                    IncidentLocation = "125 Paradise Cricle, Parklane",
                    IncidentNumber = "I20150212001",
                    IncidentRecnum = 12890,
                    IncidentStatus = "ACTIVE"
                });
                inciList.Add(new IncidentDetail()
                {
                    IncidentDate = DateTime.Today.AddDays(-29),
                    IncidentLocation = "Lane 1 RassolPura, Secunderabad",
                    IncidentNumber = "I20150212002",
                    IncidentRecnum = 12891,
                    IncidentStatus = "CLOSED"
                });
                inciList.Add(new IncidentDetail()
                {
                    IncidentDate = DateTime.Today.AddDays(-28),
                    IncidentLocation = "Lane 34 Begumpet, Secunderabad",
                    IncidentNumber = "I20150212003",
                    IncidentRecnum = 12892,
                    IncidentStatus = "PENDING INVESTIGATION"
                });
                inciList.Add(new IncidentDetail()
                {
                    IncidentDate = DateTime.Today.AddDays(-27),
                    IncidentLocation = "LifeStyle Building, Secunderabad",
                    IncidentNumber = "I20150212004",
                    IncidentRecnum = 12893,
                    IncidentStatus = "ON-VIEW ARREST"
                });
                inciList.Add(new IncidentDetail()
                {
                    IncidentDate = DateTime.Today.AddDays(-26),
                    IncidentLocation = "Hi-Tech City, Hyderabad",
                    IncidentNumber = "I20150212005",
                    IncidentRecnum = 12894,
                    IncidentStatus = "CLOSED"
                });
                inciList.Add(new IncidentDetail()
                {
                    IncidentDate = DateTime.Today.AddDays(-25),
                    IncidentLocation = "Moula-Ali, Hyderabad",
                    IncidentNumber = "I20150212006",
                    IncidentRecnum = 12896,
                    IncidentStatus = "SUSPECT SUMMONED"
                });

                // Assuming this officer has 5 Incidents data
                //for (int i = 0; i < 5; i++)
                //{
                //    inciList.Add(new IncidentDetail()
                //    {
                //        IncidentDate = DateTime.Today.AddDays(i * 100),
                //        IncidentLocation = i.ToString() + "00 WINCOPINCIRCLE COLUMBIA MARYLAND",
                //        IncidentNumber = "I2015021200" + i.ToString(),
                //        IncidentRecnum = i * 1000,
                //        IncidentStatus = "ACTIVE"
                //    });
                //}
            }
            else
            {
                // Write code to get Incidents list from RMS DB
            }

            return inciList;
        }
    }
}
