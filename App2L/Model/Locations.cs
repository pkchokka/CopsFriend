﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Geolocation;
using Windows.Foundation;
using Windows.Storage.Streams;
using Windows.UI.Xaml.Controls.Maps;

namespace CopsCompanion
{
    public class Locations
    {
        public List<MapIcon> GetLocations( bool isIncidentDetailSearch, bool isSuspect)
        {
            // We have to get the locations based Offense Recnum and Name Recnum from Incidents
            List<MapIcon> locationPointsList = new List<MapIcon>();

            if (true)
            {
                if (isIncidentDetailSearch)
                {
                    // Its a name..
                    // Check the Incidents with the Suspect as this Name and get the Incident locations
                    locationPointsList.Add(new MapIcon()
                    {
                        Title = "Incident 201404150001",
                        NormalizedAnchorPoint = new Point(0.5, 1.0),
                        Image = RandomAccessStreamReference.CreateFromUri(new Uri("ms-appx:///Assets/AssaultPin.png")),

                        //Image = new MapIconImage new Uri("ms-appx:///Assets/MapPin.png", UriKind.RelativeOrAbsolute),
                        Location = new Geopoint(new BasicGeoposition()
                        {
                            Latitude = 17.4354874,
                            Longitude = 78.4438218
                        })
                    });

                    locationPointsList.Add(new MapIcon()
                    {
                        Title = "Incident 201407150002",
                      
                        NormalizedAnchorPoint = new Point(0.5, 1.0),
                        Image = RandomAccessStreamReference.CreateFromUri(new Uri("ms-appx:///Assets/AssaultPin.png")),
                        // //ImageSourceUri = new Uri("ms-appx:///Assets/MapPin.png", UriKind.RelativeOrAbsolute),
                        Location = new Geopoint(new BasicGeoposition()
                        {
                            Latitude = 17.4509427,
                            Longitude = 78.4369473
                        })
                    });

                    locationPointsList.Add(new MapIcon()
                    {
                        Title = "Incident 201410150003",
                        NormalizedAnchorPoint = new Point(0.5, 1.0),
                        ZIndex = 0,
                        Image = RandomAccessStreamReference.CreateFromUri(new Uri("ms-appx:///Assets/AssaultPin.png")),
                        ////ImageSourceUri = new Uri("ms-appx:///Assets/MapPin.png", UriKind.RelativeOrAbsolute),
                        Location = new Geopoint(new BasicGeoposition()
                        {
                            Latitude = 17.4574779,
                            Longitude = 78.4328197
                        })
                    });

                    locationPointsList.Add(new MapIcon()
                    {
                        Title = "Incident 201501150004",
                        NormalizedAnchorPoint = new Point(0.5, 1.0),
                        Image = RandomAccessStreamReference.CreateFromUri(new Uri("ms-appx:///Assets/HomicidePin.png")),
                        // //ImageSourceUri = new Uri("ms-appx:///Assets/MapPin.png", UriKind.RelativeOrAbsolute),
                        Location =  new Geopoint(new BasicGeoposition()
                        {
                            Latitude = 17.4680305,
                            Longitude = 78.4287478
                        })
                    });

                    locationPointsList.Add(new MapIcon()
                    {
                        Title = "Incident 201504150005",
                        NormalizedAnchorPoint = new Point(0.5, 1.0),
                        ZIndex = 0,
                        Image = RandomAccessStreamReference.CreateFromUri(new Uri("ms-appx:///Assets/HomicidePin.png")),

                        //ImageSourceUri = new Uri("ms-appx:///Assets/MapPin.png", UriKind.RelativeOrAbsolute),
                        Location =  new Geopoint(new BasicGeoposition()
                        {
                            Latitude = 17.47308,
                            Longitude = 78.4248884
                        })
                    });

                    locationPointsList.Add(new MapIcon()
                    {
                        Title = "Incident 201507150006",
                        NormalizedAnchorPoint = new Point(0.5, 1.0),
                        ZIndex = 0,
                        Image = RandomAccessStreamReference.CreateFromUri(new Uri("ms-appx:///Assets/HomicidePin.png")),

                        //ImageSourceUri = new Uri("ms-appx:///Assets/MapPin.png", UriKind.RelativeOrAbsolute),
                        Location =  new Geopoint(new BasicGeoposition()
                        {
                            Latitude = 17.4875928,
                            Longitude = 78.410206
                        })
                    });

                    //locationPointsList.Add(new MapIcon()
                    //{
                    //    Title = "Incident 201510150007",
                    //    //ImageSourceUri = new Uri("ms-appx:///Assets/MapPin.png", UriKind.RelativeOrAbsolute),
                    //    Location =  new Geopoint(new BasicGeoposition()
                    //    {
                    //        Latitude = 17.494351,
                    //        Longitude = 78.402058
                    //    })
                    //});
                }

                if(isSuspect)
                {
                    // Its a offense record
                    // For testing adding few records
                    locationPointsList.Add(new MapIcon()
                    {
                        Title = "Incident 201404150001",
                        NormalizedAnchorPoint = new Point(0.5, 1.0),
                        ZIndex = 0,
                        Image = RandomAccessStreamReference.CreateFromUri(new Uri("ms-appx:///Assets/LarcenyPin.png")),

                        //ImageSourceUri = new Uri("ms-appx:///Assets/MapPin.png", UriKind.RelativeOrAbsolute), LarcenyPin.png
                        Location =  new Geopoint(new BasicGeoposition()
                        {
                            Latitude = 17.4484268,
                            Longitude = 78.5068278
                        })
                    });

                    locationPointsList.Add(new MapIcon()
                    {
                        Title = "Incident 201407150002",
                        NormalizedAnchorPoint = new Point(0.5, 1.0),
                        ZIndex = 0,
                        Image = RandomAccessStreamReference.CreateFromUri(new Uri("ms-appx:///Assets/LarcenyPin.png")),

                        //ImageSourceUri = new Uri("ms-appx:///Assets/MapPin.png", UriKind.RelativeOrAbsolute),
                        Location =  new Geopoint(new BasicGeoposition()
                        {
                            Latitude = 17.4519391,
                            Longitude = 78.4355862
                        })
                    });

                    locationPointsList.Add(new MapIcon()
                    {
                        Title = "Incident 201410150003",
                        NormalizedAnchorPoint = new Point(0.5, 1.0),
                        ZIndex = 0,
                        Image = RandomAccessStreamReference.CreateFromUri(new Uri("ms-appx:///Assets/RobberyPin.png")),
                        //ImageSourceUri = new Uri("ms-appx:///Assets/MapPin.png", UriKind.RelativeOrAbsolute),
                        Location =  new Geopoint(new BasicGeoposition()
                        {
                            Latitude = 17.436824,
                            Longitude = 78.460864
                        })
                    });

                    locationPointsList.Add(new MapIcon()
                    {
                        Title = "Incident 201501150004",
                        NormalizedAnchorPoint = new Point(0.5, 1.0),
                        ZIndex = 0,
                        Image = RandomAccessStreamReference.CreateFromUri(new Uri("ms-appx:///Assets/RobberyPin.png")),
                        //ImageSourceUri = new Uri("ms-appx:///Assets/MapPin.png", UriKind.RelativeOrAbsolute),
                        Location =  new Geopoint(new BasicGeoposition()
                        {
                            Latitude = 17.4210974,
                            Longitude = 78.4598309
                        })
                    });

                    locationPointsList.Add(new MapIcon()
                    {
                        Title = "Incident 201504150005",
                        NormalizedAnchorPoint = new Point(0.5, 1.0),
                        ZIndex = 0,
                        //BurglaryPin.png
                        Image = RandomAccessStreamReference.CreateFromUri(new Uri("ms-appx:///Assets/BurglaryPin.png")),

                        //ImageSourceUri = new Uri("ms-appx:///Assets/MapPin.png", UriKind.RelativeOrAbsolute),
                        Location =  new Geopoint(new BasicGeoposition()
                        {
                            Latitude = 17.409696,
                            Longitude = 78.4612399
                        })
                    });

                    locationPointsList.Add(new MapIcon()
                    {
                        Title = "Incident 201507150006",
                        NormalizedAnchorPoint = new Point(0.5, 1.0),
                        ZIndex = 0,
                        Image = RandomAccessStreamReference.CreateFromUri(new Uri("ms-appx:///Assets/BurglaryPin.png")),
                        //ImageSourceUri = new Uri("ms-appx:///Assets/MapPin.png", UriKind.RelativeOrAbsolute),
                        Location =  new Geopoint(new BasicGeoposition()
                        {
                            Latitude = 17.3955125,
                            Longitude = 78.4935871
                        })
                    });

                    locationPointsList.Add(new MapIcon()
                    {
                        Title = "Incident 201510150007",
                        NormalizedAnchorPoint = new Point(0.5, 1.0),
                        ZIndex = 0,
                        Image = RandomAccessStreamReference.CreateFromUri(new Uri("ms-appx:///Assets/BurglaryPin.png")),
                        //ImageSourceUri = new Uri("ms-appx:///Assets/MapPin.png", UriKind.RelativeOrAbsolute),
                        Location =  new Geopoint(new BasicGeoposition()
                        {
                            Latitude = 17.3925479,
                            Longitude = 78.486136
                        })
                    });

                    //locationPointsList.Add(new MapIcon()
                    //{
                    //    Title = "Incident 201510150007",
                    //    //ImageSourceUri = new Uri("ms-appx:///Assets/MapPin.png", UriKind.RelativeOrAbsolute),
                    //    Location =  new Geopoint(new BasicGeoposition()
                    //    {
                    //        Latitude = 17.3836224,
                    //        Longitude = 78.4869162
                    //    })
                    //});
                }                
            }
            else
            {
                // Write code to get Incident location data from RMS DB
            }
            
            return locationPointsList;
        }
    }
}
