﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CopsCompanion
{
    public class IncidentDetail
    {
        public DateTime IncidentDate { get; set; }
        public string IncidentLocation { get; set; }
        public string IncidentNumber { get; set; }        
        public int IncidentRecnum { get; set; }
        public string IncidentStatus { get; set; }
         
    }
}
